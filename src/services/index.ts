
import axios, { AxiosResponse } from 'axios'
import qs from 'qs'
import { appT } from '../types/app'

export function getDatas(type: String | undefined, score: Number | string) {
    return new Promise<appT>((resolve, reject) => {
        axios.post('/api/getDatas', qs.stringify({
            type,
            score
        })).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err)
        })
    })
}
